import React, { MouseEventHandler, useState, useEffect } from 'react'
import style from "src/styles/player.module.scss";
import {Delete, Add, PlayArrow, VolumeUp, Pause} from "@material-ui/icons";
import { CircularProgress, Slider } from '@material-ui/core';
import { Howl } from 'howler';
import * as mm from 'music-metadata-browser';

interface Props {
  trackName: string,
  isPlaying?: boolean,
  trackUrl: string,
  bpm: number,
  mode: string,
  onClick?: MouseEventHandler<SVGSVGElement>
  onPlay?: MouseEventHandler<HTMLButtonElement>
  onPause?: MouseEventHandler<HTMLButtonElement>
  onVolume?: Function,
  progress?: number
}

const Player: React.FC<Props> = ({trackName, trackUrl, isPlaying, bpm, mode, onClick, onPlay, onPause, onVolume, progress}) => {
  const [bars, setBars] = useState(0);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const body = async() => {
      //Get bars using Howler
      const metadata = new Howl({
        src: [trackUrl],
        html5: true
      });
      metadata.on("load", function(){
        setBars(Math.floor((metadata.duration() / 60) * (bpm / 4)));
        setLoading(false);
      });

      //Workaround to get song metadata, load isn't working
      metadata.play();
      metadata.pause();

      //Get metadata using FileReader
      try {
        const data = await mm.fetchFromUrl(trackUrl);
        //No metadata found
        console.log(data);
        
      } catch (error) {
        console.error(error.message);
      }
    }
    body();
    
  }, [bpm, trackUrl])
  return (
    <div className={style.container}>
      <div className={style.left}>
        <CircularProgress variant="determinate" className={style.progress} size={40} value={progress}/>
        {!isPlaying && <button className={style.button} onClick={onPlay}>
          <PlayArrow className={style.icon} />
        </button>}
        {isPlaying && <button className={style.button} onClick={onPause}>
          <Pause className={style.icon} />
        </button>}
        <div className={style.textContainer}>
          <p>{trackName}</p>
          <p className={style.bpm}>{bpm} BPM</p>
          {!loading && <p className={style.bpm}>{bars} Bars</p>}
          {loading && <p className={style.bpm}>Loading...</p>}
        </div>
      </div>

      <div className={style.right}>
        {mode === "remove" && <Delete className={style.icon} onClick={onClick}/>}
        {mode === "add" && <Add className={style.icon} onClick={onClick}/>}
        <div className={style.row}>
          <VolumeUp className={style.icon}/>
          <Slider defaultValue={100} onChange={(_, value) => onVolume && onVolume(value)} className={style.marginLeft}/>
        </div>
      </div>
      
    </div>
  )
}

export default Player
