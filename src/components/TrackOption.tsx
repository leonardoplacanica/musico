import React, { MouseEventHandler } from 'react'
import style from "src/styles/player.module.scss";
import {Add} from "@material-ui/icons";

interface Props {
  trackName: string,
  onClick?: MouseEventHandler<SVGSVGElement>
}

const Player: React.FC<Props> = ({trackName, onClick}) => {
  return (
    <div className={style.container}>
      <div className={style.left}>
        <p>{trackName}</p>
      </div>

      <div className={style.right}>
        <Add className={style.icon} onClick={onClick}/>
      </div>
    </div>
  )
}

export default Player
