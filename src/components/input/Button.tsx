import React, { MouseEventHandler } from 'react'
import style from "src/styles/button.module.scss";

interface Props {
  label: string,
  onClick: MouseEventHandler<HTMLButtonElement>,
  border?: boolean,
  selected?: boolean,
  disabled?: boolean,
}

const Button: React.FC<Props> = ({label, onClick, border = false, selected = false, disabled = false}) => {
  return (
    <button onClick={onClick} className={`${style.button} ${border && style.border} ${selected && style.selected} ${disabled && style.disabled}`}>
      {label}
    </button>
  )
}

export default Button
